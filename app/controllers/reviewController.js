const Review = require('../model/reviewModel');
const Product = require('../model/productModel');

//import thư viện mongoose
const mongoose = require('mongoose');

class ReviewController {
  getAllReview(req,res) {
    Review.find()
      .then(Reviews => res.status(200).json({
        numberReviews: Reviews.length,
        Reviews
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }
 
  getAllReviewOfProduct(req,res){
    //B1: Chuẩn bị dữ liệu
    let productId = req.params.productId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Product ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    Product.findById(productId)
    .populate('reviews')
        .exec((error, data) => {
            if(error) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return res.status(200).json({
                    status: "Get data success",
                    numberReviews: data.length, 
                    Review: data.reviews
                })
            }
        })
  }
  createReview(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.productId;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    if(!body.fullName) {
      return res.status(400).json({
        message: 'fullName is required'
      })
    }
    if(!body.email) {
      return res.status(400).json({
        message: 'email is required'
      })
    }
    if(!body.description) {
      return res.status(400).json({
        message: 'description is required'
      })
    }
    
    if(!Number.isInteger(body.rating) || body.rating < 0){
      return res.status(400).json({
        message: 'Rating is invalid!'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newReviewData = {
      _id: mongoose.Types.ObjectId(),
      fullName: body.fullName,
      email: body.email,
      rating: body.rating,
      description: body.description,
    }
    Review.create(newReviewData, (error,dataReview) =>{
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      } else {
        Product.findByIdAndUpdate(id,
          {
            $push: { reviews: dataReview._id }
          },
          (err, dataUpdate) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(201).json({
                status: "Create Review Success",
                Reviews: dataReview
              })
            }
          }
        )
      }
    })
  }

  getReviewById(req,res){
    // console.log(req.params.Reviewid);
    //B1: thu thập dữ liệu từ req
    let id = req.params.reviewId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ReviewId is invalid!'
      })
    }
    Review.findById(id)
      .then ((Review)=> res.status(200).json({
        message: "Get Review successfully",
        Review
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }

  updateReviewById(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.reviewId;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ReviewId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.fullName !== undefined && body.fullName =="") {
      return res.status(400).json({
        message: 'fullName is required'
      })
    }
    if(body.email !== undefined && body.email =="") {
      return res.status(400).json({
        message: 'email is required'
      })
    }
    if(body.description !== undefined && body.description =="") {
      return res.status(400).json({
        message: 'description is required'
      })
    }
    if(body.rating !== undefined && body.rating =="") {
      return res.status(400).json({
        message: 'rating is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let ReviewUpdate = {
      fullName: body.fullName,
      email: body.email,
      rating: body.rating,
      description: body.description,
    }
    
    if(body.fullName){
      ReviewUpdate.fullName = body.fullName;
    }
    if(body.email){
      ReviewUpdate.email = body.email;
    }
    if(body.rating){
      ReviewUpdate.rating = body.rating;
    }
    if(body.description){
      ReviewUpdate.description = body.description;
    }
    Review.findByIdAndUpdate(id,ReviewUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update Review successfully",
        Review: data
      })
    })
    
  }

  deleteReviewById(req,res){
    //B1: Chuẩn bị dữ liệu
    let ReviewId = req.params.reviewId;
    let ProductId = req.params.productId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(ReviewId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Review ID is not valid"
      })
    }
    if (!mongoose.Types.ObjectId.isValid(ProductId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Product ID is not valid"
      })
    }
    //B3: Thao tác với cơ sở dữ liệu
    Review.findByIdAndDelete(ReviewId, (error) => {
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      } else {
        //sau khi xoá xong 1 Review thì cần phải xoá ReviewID đó ra khỏi Product tương ứng        
        Product.findByIdAndUpdate(ProductId,
          {
            $pull: { reviews: ReviewId }
          },
          (err, data) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(204).json({
                status: "Success: Delete Review success"
              })
            }
          }
        )
      }
    })
    } 
}
module.exports = new ReviewController;