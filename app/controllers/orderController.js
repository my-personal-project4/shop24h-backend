const Order = require('../model/orderModel');
const Customer = require('../model/customerModel');

//import thư viện mongoose
const mongoose = require('mongoose');

class OrderController {
  async getAllOrder(req,res) {
    // Order.find()
    //   .populate({ 
    //     path: 'orderDetails',
    //     populate:
    //     {
    //       path: 'product',
    //       populate: {
    //         path: "type",
    //         model: "ProductType"
    //       }
    //     }
    //   })
    //   .then(orders => res.status(200).json({
    //     numberOrders: orders.length,
    //     orders
    //   }))
    //   .catch(error=>{
    //     res.status(500).json({
    //       message: error.message
    //     })
    //   });
    try {
      const page = parseInt(req.query.page) - 1 || 0;
      const limit = parseInt(req.query.limit) || 6;
      let {customerFilter} = req.query
      var regex = new RegExp( customerFilter, 'i');
      let condition = {}
      if(customerFilter !== undefined) {
        condition.fullName = {$regex: regex}
      }
      const order = await Order.find(condition)
      .skip(page * limit)
      .limit(limit)
      .populate({ 
        path: 'orderDetails',
        populate:
        {
          path: 'product',
          populate: {
            path: "type",
            model: "ProductType"
          }
        }
      })
      const total =  await  Order.countDocuments(condition)
      const response = {
          message: `get all order success`,
          total,
          page: page + 1,
          limit,
          order,
      };
      res.status(200).json(response);
      // console.log(response)
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: true, message: "Internal Server Error" });
    }
  }
 
  getAllOrderOfCustomer(req,res){
    //B1: Chuẩn bị dữ liệu
    let customerId = req.params.customerid;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Customer ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    Customer.findById(customerId)
        .populate({ 
          path: 'orders',
          populate:
            {
              path: 'orderDetails',
              populate: {
                path: 'product',
                populate: {
                  path: "type",
                  model: "ProductType"
                }
              }
            }, 
        })
        .exec((error, data) => {
            if(error) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return res.status(200).json({
                    status: "Get data success",
                    name: data.fullName,
                    numberOrders: data.length, 
                    order: data.orders
                })
            }
        })
  }

  // getAllOrderOfCustomerTest(req,res){
  //   //B1: Chuẩn bị dữ liệu
  //   // let customerId = req.params.customerid;
  //   // //B2: Validate dữ liệu
  //   // if(!mongoose.Types.ObjectId.isValid(customerId)) {
  //   //     return response.status(400).json({
  //   //         status: "Error 400: Bad Request",
  //   //         message: "Customer ID is invalid"
  //   //     })
  //   // }
  //   //B3: Thao tác với cơ sở dữ liệu
  //   Customer.find({email: req.email})
  //       .populate({ 
  //         path: 'orders',
  //         populate:
  //           {
  //             path: 'orderDetails',
  //             populate: {
  //               path: 'product',
  //               populate: {
  //                 path: "type",
  //                 model: "ProductType"
  //               }
  //             }
  //           }, 
  //       })
  //       .exec((error, data) => {
  //           if(error) {
  //               return res.status(500).json({
  //                   status: "Error 500: Internal server error",
  //                   message: error.message
  //               })
  //           } else {
  //             console.log(data)
  //               return res.status(200).json({
  //                   status: "Get data success",
  //                   name: data.fullName,
  //                   numberOrders: data.length, 
  //                   order: data.orders
  //               })
  //           }
  //       })
  // }

  createOrder(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.customerid;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'CustomerId is invalid!'
      })
    }
    if(!Number.isInteger(body.cost) || body.cost < 0){
      return res.status(400).json({
        message: 'cost is invalid!'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newOrderData = {
      _id: mongoose.Types.ObjectId(),
      orderDate: body.orderDate,
      shippedDate: body.shippedDate,
      note: body.note,
      // orderDetails: body.orderDetails,
      fullName: body.fullName,
      phone: body.phone,
      email: body.email,
      address: body.address,
      city: body.city,
      country: body.country,
      cost: body.cost,
      statusOrder: body.statusOrder,
    }
    Order.create(newOrderData, (error,dataOrder) =>{
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      }
       else {
        Customer.findByIdAndUpdate(id,
          {
            $push: { orders: dataOrder._id }
          },
          (err, data) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(201).json({
                status: "Create Order Success",
                order: dataOrder
              })
            }
          }
        )
      }
    })
  }

  getOrderById(req,res){
    // console.log(req.params.Orderid);
    //B1: thu thập dữ liệu từ req
    let id = req.params.orderid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'OrderId is invalid!'
      })
    }
    Order.findById(id)
      .populate({ 
        path: 'orderDetails',
        populate:
          {
            path: 'product',
            model: 'Product'
          }, 
      })
      .then ((order)=> res.status(200).json({
        message: "Get order successfully",
        order
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }
                 
  updateOrderById(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.orderid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'OrderId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.cost !== undefined && body.cost =="") {
      return res.status(400).json({
        message: 'cost is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let orderUpdate = {
      orderDate: body.orderDate,
      shippedDate: body.shippedDate,
      note: body.note,
      fullName: body.fullName,
      phone: body.phone,
      email: body.email,
      address: body.address,
      city: body.city,
      country: body.country,
      cost: body.cost,
      statusOrder: body.statusOrder,
    }
    if(body.orderDate){
      orderUpdate.orderDate = body.orderDate;
    }
    if(body.shippedDate){
      orderUpdate.shippedDate = body.shippedDate;
    }
    if(body.note){
      orderUpdate.note = body.note;
    }
    if(body.cost){
      orderUpdate.cost = body.cost;
    }
    Order.findByIdAndUpdate(id,orderUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update order successfully",
        order: data
      })
    })
    
  }

  deleteOrderById(req,res){
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderid;
    let customerId = req.params.customerid;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Order ID is not valid"
      })
    }

    if (!mongoose.Types.ObjectId.isValid(customerId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "Customer ID is not valid"
      })
    }
    //B3: Thao tác với cơ sở dữ liệu
    Order.findByIdAndDelete(orderId, (error) => {
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      } else {
        //sau khi xoá xong 1 order thì cần phải xoá orderID đó ra khỏi Customer tương ứng        
        Customer.findByIdAndUpdate(customerId,
          {
            $pull: { orders: orderId }
          },
          (err, data) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(204).json({
                status: "Success: Delete order success"
              })
            }
          }
        )
      }
    })
    } 
}
module.exports = new OrderController;