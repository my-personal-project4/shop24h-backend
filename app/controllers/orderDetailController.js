const OrderDetail = require('../model/orderDetailModel');
const Order = require('../model/orderModel');

//import thư viện mongoose
const mongoose = require('mongoose');

class OrderDetailController {
  getAllOrderDetail(req,res) {
    OrderDetail.find()
    .populate('product')
      .then(OrderDetails => res.status(200).json({
        numberOrderDetails: OrderDetails.length,
        OrderDetails
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }
 
  getAllOrderDetailOfOrder(req,res){
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderid;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "order ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    Order.findById(orderId)
    .populate({ 
      path: 'orderDetails',
        populate:
        {
          path: 'product',
          populate: {
            path: "type",
            model: "ProductType"
          }
        }
    })
        .exec((error, data) => {
            if(error) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return res.status(200).json({
                    status: "Get data success",
                    numberOrderDetails: data.length, 
                    OrderDetail: data.orderDetails
                })
            }
        })
  }
  createOrderDetail(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.orderid;
    let body = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'orderId is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.product)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    if(!Number.isInteger(body.quantity) || body.quantity < 0){
      return res.status(400).json({
        message: 'quantity is invalid!'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newOrderDetailData = {
      _id: mongoose.Types.ObjectId(),
      product: body.product,
      quantity: body.quantity,
    }
    OrderDetail.create(newOrderDetailData, (error,dataOrderDetail) =>{
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      } else {
        Order.findByIdAndUpdate(id,
          {
            $push: { orderDetails: dataOrderDetail._id }
          },
          (err, dataUpdate) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(201).json({
                status: "Create OrderDetail Success",
                orderDetails: dataOrderDetail
              })
            }
          }
        )
      }
    })
  }

  getOrderDetailById(req,res){
    // console.log(req.params.OrderDetailid);
    //B1: thu thập dữ liệu từ req
    let id = req.params.orderDetailid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'OrderDetailId is invalid!'
      })
    }
    OrderDetail.findById(id)
      .populate({
        path: 'product',
        populate: {
          path: "type",
          model: "ProductType"
        }
      })
      .then ((OrderDetail)=> res.status(200).json({
        message: "Get OrderDetail successfully",
        OrderDetail
      }))
      .catch(error=>{
        res.status(500).json({
          message: error.message
        })
      });
  }

  updateOrderDetailById(req,res){
    //B1: thu thập dữ liệu từ req
    let id = req.params.orderDetailid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'OrderDetailId is invalid!'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.product)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.quantity !== undefined && body.quantity =="") {
      return res.status(400).json({
        message: 'quantity is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let orderDetailUpdate = {
      product: body.product,
      quantity: body.quantity,
    }
    if(body.product){
      orderDetailUpdate.product = body.product;
    }
    if(body.quantity){
      orderDetailUpdate.quantity = body.quantity;
    }
    OrderDetail.findByIdAndUpdate(id,orderDetailUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update OrderDetail successfully",
        OrderDetail: data
      })
    })
    
  }

  deleteOrderDetailById(req,res){
    //B1: Chuẩn bị dữ liệu
    let orderDetailId = req.params.orderDetailid;
    let orderId = req.params.orderid;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "OrderDetail ID is not valid"
      })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      return response.status(400).json({
        status: "Error 400: Bad Request",
        message: "order ID is not valid"
      })
    }
    //B3: Thao tác với cơ sở dữ liệu
    OrderDetail.findByIdAndDelete(orderDetailId, (error) => {
      if (error) {
        return res.status(500).json({
          status: "Error 500: Internal server error",
          message: error.message
        })
      } else {
        //sau khi xoá xong 1 OrderDetail thì cần phải xoá OrderDetailID đó ra khỏi order tương ứng        
        Order.findByIdAndUpdate(orderId,
          {
            $pull: { orderDetails: orderDetailId }
          },
          (err, data) => {
            if (err) {
              return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
              })
            } else {
              return res.status(204).json({
                status: "Success: Delete OrderDetail success"
              })
            }
          }
        )
      }
    })
    } 
}
module.exports = new OrderDetailController;