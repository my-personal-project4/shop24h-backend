const ProductType = require("../model/productTypeModel");
const Product = require("../model/productModel");
//import thư viện mongoose
const mongoose = require("mongoose");

class ProductTypeController {
  //
  getAllProductType(req, res) {
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    ProductType.find((error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }

      return res.status(200).json({
        message: "Get all ProductTypes successfully",
        numberProductTypes: data.length,
        ProductTypes: data,
      });
    });
  }

  async getAllProductTypeTest(req, res) {
    try {
      const page = parseInt(req.query.page) - 1 || 0;
      const limit = parseInt(req.query.limit) || 8;
      let { productTypeName, type } = req.query;
      var regex = new RegExp(productTypeName, "i");
      let condition = {};
      if (productTypeName) {
        condition.name = { $regex: regex };
      }
      const productType = await ProductType.find(condition)
        .skip(page * limit)
        .limit(limit);
      const total = await ProductType.countDocuments(condition)  
      const totalProduct = await Product.find({type: type})
      const response = {
        message: `get all productTypes success`,
        total,
        numberTotalProduct: totalProduct.length,
        totalProduct,
        page: page + 1,
        limit,
        productType,
    };

    res.status(200).json(response);
    } catch (error) {
      console.log(error);
		  res.status(500).json({ error: true, message: "Internal Server Error" });
    }
  }

  createProductType(req, res) {
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if (!body.name) {
      return res.status(400).json({
        message: "Mã nước uống phải nhập",
      });
    }

    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newProductTypeData = {
      _id: mongoose.Types.ObjectId(),
      name: body.name,
      description: body.description,
    };
    ProductType.create(newProductTypeData, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }

      return res.status(201).json({
        message: "Create successfully",
        newProductType: data,
      });
    });
  }

  getProductTypeById(req, res, next) {
    //B1: thu thập dữ liệu từ req
    let id = req.params.productTypeid;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: "ProductTypeId is invalid!",
      });
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    ProductType.findById(id, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      if (data === null)
        return res.status(404).json({
          notFound: "ID not found!",
        });
      return res.status(200).json({
        message: "Get ProductType successfully",
        ProductType: data,
      });
    });
  }

  updateProductTypeById(req, res, next) {
    // ProductType.updateOne({_id: req.params.ProductTypeid}, req.body)
    // .then((ProductType)=> res.status(200).json(ProductType))
    // .catch(next)

    //B1: thu thập dữ liệu từ req
    let id = req.params.productTypeid;
    let body = req.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: "ProductTypeId is invalid!",
      });
    }
    //bóc tách trường hợp undefined
    if (body.name !== undefined && body.name == "") {
      return res.status(400).json({
        message: "Name is required",
      });
    }
    // if(body.description !== undefined && body.description =="") {
    //   return res.status(400).json({
    //     message: 'Description'
    //   })
    // }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let productTypeUpdate = {
      name: body.name,
      description: body.description,
    };
    if (body.name) {
      productTypeUpdate.name = body.name;
    }
    if (body.description) {
      productTypeUpdate.description = body.description;
    }

    ProductType.findByIdAndUpdate(id, productTypeUpdate, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }

      return res.status(200).json({
        message: "Update ProductType successfully",
        ProductType: data,
      });
    });
  }

  deleteProductTypeById(req, res, next) {
    // ProductType.findByIdAndDelete(req.params.ProductTypeid)
    //   .then(()=> res.status(200).json({
    //     message: `Delete successfully id = ${req.params.ProductTypeid}`
    //   }))
    //   .catch(next);
    //B1: thu thập dữ liệu từ req
    let id = req.params.productTypeid;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: "ProductTypeId is invalid!",
      });
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    ProductType.findByIdAndDelete(id, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      if (data === null)
        return res.status(404).json({
          notFound: "ID not found!",
        });
      return res.status(204).json({
        message: "Delete ProductType successfully",
      });
    });
  }
}
module.exports = new ProductTypeController();
