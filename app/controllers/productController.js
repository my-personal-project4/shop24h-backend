const Product = require('../model/productModel');
//import thư viện mongoose
const mongoose = require('mongoose');
class ProductController {
  async getAllProduct(req,res) {
    try {
    const page = parseInt(req.query.page) - 1 || 0;
    const limit = parseInt(req.query.limit) || 8;
    let {productName, minPrice, maxPrice, type, color, size} = req.query;
    var regex = new RegExp( productName, 'i');
    let condition = {};
    if(productName){
        condition.name = {$regex: regex}
    }
    if(color !== undefined && !Array.isArray(color) && color!== "ALL")  {
      color = [color]
    }
    
    if(type !== undefined && type!== "ALL"){
      condition.type= type
    }

    if(Array.isArray(color)) {
        condition.color = {
            $in: color
        }
    }
    if(size !== undefined && !Array.isArray(size)&& size!== "ALL") {
      size = [size]
    }

    if(Array.isArray(size)) {
        condition.size = {
            $in: size
        }
    }
    if(minPrice){
        condition.buyPrice = {
            ...condition.buyPrice,
            $gte : minPrice
        }
    }
    if(maxPrice){
        condition.buyPrice = {
          ...condition.buyPrice,
           $lte : maxPrice,
        }
    }
    // const params = new URLSearchParams({
    //   var1: "value",
    //   var2: "value2",
    //   arr: [1,2,3],
    // });
    // console.log(params.toString());
    const product = await Product.find(condition)
    .skip(page * limit)
    .limit(limit)
    .populate("type")
    .populate('reviews')
    const total =  await  Product.countDocuments(condition)
    const response = {
        message: `get all products success`,
        total,
        // numberProduct: product1,
        page: page + 1,
        limit,
        product,
    };

    res.status(200).json(response);
    } catch (error) {
      console.log(error);
		  res.status(500).json({ error: true, message: "Internal Server Error" });
    }
  }
        
        
    getProductRandom(req,res) {
      Product.aggregate([{$sample:{size:8}}]).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all products success`,
            numberProduct: data.length,
            product: data
        })
    })
  }

  getAllProducts(req,res) {
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Product.find()
      .populate('type')
      .populate('reviews')
      .exec((error,data)=> {
        if(error) {
          return res.status(500).json({
            message: error.message
          })
        }
        
        return res.status(200).json({
          message: "Get all Products successfully",
          numberProducts: data.length,
          Products: data
        })
      })
  }

  createProduct(req,res,next){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!body.name) {
      return res.status(400).json({
        message: 'Name is requires'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.type)) {
      return res.status(400).json({
        message: 'Type is invalid!'
      })
    }
    if(!body.imageUrl) {
      return res.status(400).json({
        message: 'imageUrl is invalid!'
      })
    }
    if(!Number.isInteger(body.buyPrice) || body.buyPrice < 0){
      return res.status(400).json({
        message: 'buyPrice is invalid!'
      }) 
    }
    if(!Number.isInteger(body.promotionPrice) || body.promotionPrice < 0){
      return res.status(400).json({
        message: 'promotionPrice is invalid!'
      }) 
    }
    if(!Number.isInteger(body.amount) || body.amount < 0){
      return res.status(400).json({
        message: 'amount is invalid!'
      }) 
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newProductData = {
      _id: mongoose.Types.ObjectId(),
      name: body.name,
      description: body.description,
      type: body.type,
      imageUrl: body.imageUrl,
      imageChild: body.imageChild,
      size: body.size,
      color: body.color,
      buyPrice: body.buyPrice,
      promotionPrice: body.promotionPrice,
      amount: body.amount,
    
    }
    Product.create(newProductData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newProduct: data
      })
    })
  }

  getProductById(req,res,next){
    // Product.findById(req.params.Productid)
    //   .then ((Product)=> res.status(200).json(Product))
    //   .catch(next);

    //B1: thu thập dữ liệu từ req
    let id = req.params.productid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Product.findById(id)
    .populate("type").populate('reviews').exec((error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get Product successfully",
        Product: data
      })
    })  
  }

  updateProductById(req,res,next){
    // Product.updateOne({_id: req.params.Productid}, req.body)
    // .then((Product)=> res.status(200).json(Product))
    // .catch(next)

    //B1: thu thập dữ liệu từ req
    let id = req.params.productid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.name !== undefined && body.name =="") {
      return res.status(400).json({
        message: 'Name is required'
      })
    }
    if(!mongoose.Types.ObjectId.isValid(body.type)) {
      return res.status(400).json({
        message: 'Type is invalid!'
      })
    }
    if(body.imageUrl !== undefined && body.imageUrl =="") {
      return res.status(400).json({
        message: 'imageUrl is required'
      })
    }
    if(body.buyPrice !== undefined && body.buyPrice =="") {
      return res.status(400).json({
        message: 'buyPrice is required'
      })
    }
    if(body.promotionPrice !== undefined && body.promotionPrice =="") {
      return res.status(400).json({
        message: 'promotionPrice is required'
      })
    }
    if(body.amount !== undefined && body.amount =="") {
      return res.status(400).json({
        message: 'amount is required'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let productUpdate = {
      name: body.name,
      description: body.description,
      imageUrl: body.imageUrl,
      imageChild: body.imageChild,
      size: body.size,
      color: body.color,
      buyPrice: body.buyPrice,
      promotionPrice: body.promotionPrice,
      amount: body.amount,
    }
    if(body.name){
      productUpdate.name = body.name;
    }
    if(body.description){
      productUpdate.description = body.description;
    }
    if(body.type){
      productUpdate.type = body.type;
    }
    if(body.imageUrl){
      productUpdate.imageUrl = body.imageUrl;
    }
    if(body.buyPrice){
      productUpdate.buyPrice = body.buyPrice;
    }
    if(body.promotionPrice){
      productUpdate.promotionPrice = body.promotionPrice;
    }
    if(body.amount){
      productUpdate.amount = body.amount;
    }
   
    Product.findByIdAndUpdate(id,productUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update Product successfully",
        Product: data
      })
    })
  }

  deleteProductById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.productid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'ProductId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Product.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete Product successfully",
      })
    })
  }
} 
module.exports = new ProductController;