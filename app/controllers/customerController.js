const Customer = require('../model/customerModel');
//import thư viện mongoose
const mongoose = require('mongoose');
class CustomerController {
   getAllCustomer(req,res) {
    // try {
    //   const page = parseInt(req.query.page) - 1 || 0;
    //   const limit = parseInt(req.query.limit) || 8;
    //   let {phoneNumber} = req.query
    //   var regex = new RegExp( phoneNumber, 'i');
    //   let condition = {}
    //   if(phoneNumber !== undefined) {
    //     condition.phone = {$regex: regex}
    //   }
    //   const customer = await Customer.find(condition)
    //   .skip(page * limit)
    //   .limit(limit)
    //   .populate({ 
    //     path: 'orders',
    //     populate:
    //       {
    //         path: 'orderDetails',
    //         populate: {
    //           path: 'product',
    //           populate: {
    //             path: "type",
    //             model: "ProductType"
    //           }
    //         }
            
    //       }, 
    //   })
    //   const total =  await  Customer.countDocuments(condition)
    //   const response = {
    //       message: `get all customer success`,
    //       total,
    //       // numberProduct: product1,
    //       page: page + 1,
    //       limit,
    //       customer,
    //   };
    //   res.status(200).json(response);
    // } catch (error) {
    //     console.log(error);
    //     res.status(500).json({ error: true, message: "Internal Server Error" });
    // }
    // B1: thu thập dữ liệu từ req
    // B2: validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let phone = req.query.phoneNumber
    let condition = {}
    if(phone !== undefined) {
      condition.phone = phone
    }
    Customer.find(condition)
    .populate({ 
      path: 'orders',
      populate:
        {
          path: 'orderDetails',
          populate: {
            path: 'product',
            populate: {
              path: "type",
              model: "ProductType"
            }
          }
          
        }, 
    })
    .then(Customers => res.status(200).json({
      numberCustomers: Customers.length,
      Customers
    }))
    .catch(error=>{
      res.status(500).json({
        message: error.message
      })
    });
    
  }

  async testCustomer(req,res){
    try {
          const page = parseInt(req.query.page) - 1 || 0;
          const limit = parseInt(req.query.limit) || 8;
          let {phoneNumber} = req.query
          var regex = new RegExp( phoneNumber, 'i');
          let condition = {}
          if(phoneNumber !== undefined) {
            condition.phone = {$regex: regex}
          }
          const customer = await Customer.find(condition)
          .skip(page * limit)
          .limit(limit)
          .populate({ 
            path: 'orders',
            populate:
              {
                path: 'orderDetails',
                populate: {
                  path: 'product',
                  populate: {
                    path: "type",
                    model: "ProductType"
                  }
                }
                
              }, 
          })
          const total =  await  Customer.countDocuments(condition)
          const response = {
              message: `get all customer success`,
              total,
              // numberProduct: product1,
              page: page + 1,
              limit,
              customer,
          };
          res.status(200).json(response);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: true, message: "Internal Server Error" });
    }
  }
  createCustomer(req,res){
    //B1: thu thập dữ liệu từ req
    let body = req.body;
    // console.log(body);
    //B2: validate dữ liệu
    if(!body.fullName) {
      return res.status(400).json({
        message: 'fullName is required'
      })
    }
    if(!body.email) {
      return res.status(400).json({
        message: 'email is required'
      })
    }
    if(!body.phone) {
      return res.status(400).json({
        message: 'phone is required'
      })
    }
  
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newCustomerData = {
      _id: mongoose.Types.ObjectId(),
      fullName: body.fullName,
      email: body.email,
      phone: body.phone,
      address: body.address,
      city: body.city,
      country: body.country,
      message: body.message,
    }
    Customer.create(newCustomerData, (error,data) =>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(201).json({
        message: "Create successfully",
        newCustomer: data
      })
    })
  }

  async getAllOrderOfCustomerTest(req,res){
    try {
      const page = parseInt(req.query.page) - 1 || 0;
      const limit = parseInt(req.query.limit) || 6;
      
      const customer= await Customer.find({email: req.email})
          .populate({ 
            path: 'orders',
            populate:
              {
                path: 'orderDetails',
                populate: {
                  path: 'product',
                  populate: {
                    path: "type",
                    model: "ProductType"
                  }
                }
              }, 
          })
          const total = await  Customer.countDocuments({email: req.email})
          const response = {
              message: `get all customer success`,
              total,
              // numberProduct: product1,
              page: page + 1,
              limit,
              customer,
          };
          
          res.status(200).json(response);
          // .exec((error, data) => {
          //     if(error) {
          //         return res.status(500).json({
          //             status: "Error 500: Internal server error",
          //             message: error.message
          //         })
          //     } else {
          //       console.log(data)
          //         return res.status(200).json({
          //             status: "Get data success",
          //             Customer:data
          //         })
          //     }
          // })
    } catch (error) {
      console.log(error);
        res.status(500).json({ error: true, message: "Internal Server Error" });
    }
  }

  getCustomerById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.customerid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'CustomerId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Customer.findById(id)
    .populate({ 
      path: 'orders',
      populate:
        {
          path: 'orderDetails',
          populate: {
            path: 'product',
            populate: {
              path: "type",
              model: "ProductType"
            }
          }
          
        }, 
    })
      .exec((error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(200).json({
        message: "Get Customer successfully",
        Customer: data
      })
    })
  }

  updateCustomerById(req,res,next){
    //B1: thu thập dữ liệu từ req
    let id = req.params.customerid;
    let body = req.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'CustomerId is invalid!'
      })
    }
    //bóc tách trường hợp undefined
    if(body.fullName !== undefined && body.fullName =="") {
      return res.status(400).json({
        message: 'fullName is required'
      })
    }
    if(body.email !== undefined && body.email =="") {
      return res.status(400).json({
        message: 'email is required'
      })
    }
   
    if(body.phone !== undefined && body.phone =="") {
      return res.status(400).json({
        message: 'phone is required'
      })
    }
    
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let customerUpdate = {
      fullName: body.fullName,
      email: body.email,
      address: body.address,
      phone: body.phone,
      city: body.city,
      country: body.country,
      message: body.message,
    }
    if(body.fullName){
      customerUpdate.fullName = body.fullName;
    }
    if(body.email){
      customerUpdate.email = body.email;
    }
    if(body.address){
      customerUpdate.address = body.address;
    }
    if(body.phone){
      customerUpdate.phone = body.phone;
    }
    if(body.country){
      customerUpdate.country = body.country;
    }
    if(body.city){
      customerUpdate.city = body.city;
    }
    if(body.message){
      customerUpdate.message = body.message;
    }

    Customer.findByIdAndUpdate(id,customerUpdate,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }

      return res.status(200).json({
        message: "Update Customer successfully",
        Customer: data
      })
    })
  }

  deleteCustomerById(req,res,next){
    
    //B1: thu thập dữ liệu từ req
    let id = req.params.customerid;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({
        message: 'CustomerId is invalid!'
      })
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    Customer.findByIdAndDelete(id,(error,data)=>{
      if(error) {
        return res.status(500).json({
          message: error.message
        })
      }
      if (data === null) return res.status(404).json({
        notFound: "ID not found!"
      })
      return res.status(204).json({
        message: "Delete Customer successfully",
      })
    })
  }
} 
module.exports = new CustomerController;