const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Product = new Schema({
	// _id: {type: ObjectId, unique:true},
	name: {type: String, unique:true, required:true},
	description: {type: String},
	type: {type: mongoose.Types.ObjectId,ref:"ProductType", required:true},
	imageUrl: {type: String, require: true},
	imageChild: [{type: String}],
	size: [{type: String}],
	color: [{type: String}],
	buyPrice: {type: Number, require: true},
	promotionPrice: {type: Number, require: true},
	amount: {type: Number, default: 0},
	reviews: [{
		type: mongoose.Types.ObjectId,
		ref:"Review"
	}],
},{
	timestamps:true
});

module.exports = mongoose.model('Product', Product);
  