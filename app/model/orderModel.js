const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

//khai báo random orderCode
function generateString(length) {
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

const Order = new Schema({
	// _id: {type: Schema.Types.ObjectId, unique:true},
	orderDate: {type: Date, unique:true, default: () => Date.now()},
	shippedDate: {type: Date},
	note: {type: String},
	orderDetails: [{
		type: mongoose.Types.ObjectId,
		ref:"OrderDetail"
	}],
	fullName: {type: String, required:true},
	phone: {type: String, required:true},
	email: {type: String, required:true,},
	address: {type: String, default:""},
	city: {type: String, default:""},
	country: {type: String, default:""},
	cost: {type: Number, default: 0},
	orderCode: {type: String,  default: () => generateString(6)},
	statusOrder: {type: String, default: "Processing"}
},{
	timestamps: true
});

module.exports = mongoose.model('Order', Order);
 