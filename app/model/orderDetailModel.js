const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderDetail = new Schema({
	// _id: {type: ObjectId, unique:true},
	product: {
		type: mongoose.Types.ObjectId,
		ref:"Product"
	},
	quantity: {type: Number, default: 0},
},{
	timestamps:true
});

module.exports = mongoose.model('OrderDetail', OrderDetail);
 