const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Review = new Schema({
	// _id: {type: ObjectId, unique:true},
	fullName: {type: String, required:true},
	email: {type: String, required:true},
	rating: {type: Number, required:true},
	description: {type: String, required: true}
},{
	timestamps:true
});

module.exports = mongoose.model('Review', Review);
 