//khai báo thư viện express
const express = require('express');
const productMiddleware = require('../middlewares/productMiddleware');
const productController = require('../controllers/productController');
//tạo router
const productRouter = express.Router();

//sủ dụng middle ware
productRouter.use(productMiddleware);

//get all products with limit
productRouter.get('/products', productController.getAllProduct);
//get all products 
productRouter.get('/products-all', productController.getAllProducts);

//get random products
productRouter.get('/products-random', productController.getProductRandom);

//get a product
productRouter.get('/products/:productid', productController.getProductById);

//create a product
productRouter.post('/products', productController.createProduct);


//update a product
productRouter.put('/products/:productid',productController.updateProductById);

//delete a product
productRouter.delete('/products/:productid',productController.deleteProductById);

module.exports = { productRouter };