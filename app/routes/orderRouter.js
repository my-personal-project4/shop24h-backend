//khai báo thư viện express
const express = require('express');
const orderMiddleware = require('../middlewares/orderMiddleware');
const orderController = require('../controllers/orderController');
const authMiddleware = require('../middlewares/auth-middleware')
//tạo router
const orderRouter = express.Router();

//sủ dụng middle ware
// orderRouter.use(orderMiddleware);

//get all orders

orderRouter.get('/orders', orderController.getAllOrder);

//get all orders of customer
orderRouter.get("/customers/:customerid/orders", orderController.getAllOrderOfCustomer);

// orderRouter.get("/orders-test",authMiddleware, orderController.getAllOrderOfCustomerTest);

//get a order
orderRouter.get('/orders/:orderid', orderController.getOrderById);

//create a order
orderRouter.post('/customers/:customerid/orders', orderController.createOrder);

//update a order
orderRouter.put('/orders/:orderid', orderController.updateOrderById);

//delete a order
orderRouter.delete('/customers/:customerid/orders/:orderid',orderController.deleteOrderById);

module.exports = { orderRouter };