//khai báo thư viện express
const express = require('express');
const { productTypeMiddleware } = require('../middlewares/productTypeMiddleware');
// const productType = require('../model/productTypeModel');
const productTypeController = require('../controllers/productTypeController');
//tạo router
const productTypeRouter = express.Router();

 
//sủ dụng middle ware
productTypeRouter.use(productTypeMiddleware);
 
//get all productTypes
productTypeRouter.get('/productTypes',productTypeController.getAllProductType);
productTypeRouter.get('/productTypesTest',productTypeController.getAllProductTypeTest);

//get a productType
// productTypeRouter.get('/productTypes/:productTypeid', (request, response) => {
//     let id = request.params.productTypeid;

//     console.log(`Get All productTypeId = ${id}`);
//     response.json({
//         message:`Get All productTypeId = ${id}`
//     })
// })
//get a productType
productTypeRouter.get('/productTypes/:productTypeid', productTypeController.getProductTypeById);
 
//create a productType
// productTypeRouter.post('/productTypes', (request, response) => {
//     let body = request.body;

//     console.log('create a productType');
//     console.log(body);
//     response.json({
//         ...body
//     })
// });
productTypeRouter.post('/productTypes', productTypeController.createProductType);


//update a productType
// productTypeRouter.put('/productTypes/:productTypeid', (request, response) => {
//     let id = request.params.productTypeid;
//     let body = request.body;

//     console.log('update a productType');
//     console.log({id, ...body});
//     response.json({
//         message: {id, ...body}
//     })
// })
productTypeRouter.put('/productTypes/:productTypeid', productTypeController.updateProductTypeById);

//delete a course
// productTypeRouter.delete('/productTypes/:productTypeid', (request, response) => {
//     let id = request.params.productTypeid;

//     console.log('delete a productType' + id);
//     response.json({
//         message: 'delete a productType' + id
//     })
// })
productTypeRouter.delete('/productTypes/:productTypeid', productTypeController.deleteProductTypeById)

module.exports = { productTypeRouter };