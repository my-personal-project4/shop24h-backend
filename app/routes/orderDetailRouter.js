//khai báo thư viện express
const express = require('express');
const orderMiddleware = require('../middlewares/orderDetailMiddleware');
const orderDetailController = require('../controllers/orderDetailController');
//tạo router
const orderDetailRouter = express.Router();

//sủ dụng middle ware
orderDetailRouter.use(orderMiddleware);

//get all orders

orderDetailRouter.get('/orderDetails', orderDetailController.getAllOrderDetail);

//get all orderDetails of order
orderDetailRouter.get("/orders/:orderid/orderDetails", orderDetailController.getAllOrderDetailOfOrder);

//get a orderDetail
orderDetailRouter.get('/orderDetails/:orderDetailid', orderDetailController.getOrderDetailById);

//create a order
orderDetailRouter.post('/orders/:orderid/orderDetails', orderDetailController.createOrderDetail);

//update a order
orderDetailRouter.put('/orderDetails/:orderDetailid', orderDetailController.updateOrderDetailById);

//delete a order
orderDetailRouter.delete('/orders/:orderid/orderDetails/:orderDetailid',orderDetailController.deleteOrderDetailById);

module.exports = { orderDetailRouter };