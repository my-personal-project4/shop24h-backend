//khai báo thư viện express
const express = require('express');
// const orderMiddleware = require('../middlewares/orderDetailMiddleware');
const reviewController = require('../controllers/reviewController');
//tạo router
const reviewRouter = express.Router();

//sủ dụng middle ware
// reviewRouter.use(orderMiddleware);

//get all products

reviewRouter.get('/reviews', reviewController.getAllReview);

//get all reviews of review
reviewRouter.get("/products/:productId/reviews", reviewController.getAllReviewOfProduct);

//get a Review
reviewRouter.get('/reviews/:reviewId', reviewController.getReviewById);

//create a review
reviewRouter.post('/products/:productId/reviews', reviewController.createReview);

//update a review
reviewRouter.put('/reviews/:reviewId', reviewController.updateReviewById);

//delete a review
reviewRouter.delete('/products/:productId/reviews/:reviewId',reviewController.deleteReviewById);

module.exports = { reviewRouter };