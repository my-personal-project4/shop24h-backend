//khai báo thư viện express
const express = require('express');
const customerMiddleware = require('../middlewares/customerMiddleware');
const customerController = require('../controllers/customerController');
const authMiddleware = require('../middlewares/auth-middleware')
//tạo router
const customerRouter = express.Router();

//sủ dụng middle ware
// customerRouter.use(customerMiddleware);

//get all customers
customerRouter.get('/customers', customerController.getAllCustomer);
customerRouter.get('/customers-test', customerController.testCustomer);

//get a customer
customerRouter.get('/customers/:customerid', customerController.getCustomerById);
customerRouter.get('/customers-check',authMiddleware, customerController.getAllOrderOfCustomerTest);

//create a customer
customerRouter.post('/customers',customerController.createCustomer);

//update a customer
customerRouter.put('/customers/:customerid',customerController.updateCustomerById);

//delete a customer
customerRouter.delete('/customers/:customerid',customerController.deleteCustomerById);

module.exports = { customerRouter };