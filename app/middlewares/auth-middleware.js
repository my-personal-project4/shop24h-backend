const firebase = require("../../firebase/admin");

function authMiddleware(request, response, next) {
  const headerToken = request.headers.authorization;
  if (!headerToken) {
    return response.status(401).send({ message: "No token provided" });
  }

  if (headerToken && headerToken.split(" ")[0] !== "Bearer") {
    return response.status(401).send({ message: "Invalid token" });
  }

  const token = headerToken.split(" ")[1];

  firebase
    .auth()
    .verifyIdToken(token)
    .then((decodeToken) => {
      // Validate user on database
      console.log(decodeToken.email)
      request.email = decodeToken.email
      next()
    })
    .catch(() => response.status(403).send({ message: "Could not authorize" }));
}

module.exports = authMiddleware;
