//khai báo thư viện express
const express = require('express');
var cors = require('cors')
const { productTypeRouter } = require('./app/routes/productTypeRouter');
const { productRouter } = require('./app/routes/productRouter');
const { customerRouter } = require('./app/routes/customerRouter');
const { orderRouter } = require('./app/routes/orderRouter');
const { orderDetailRouter } = require('./app/routes/orderDetailRouter');
const { reviewRouter } = require('./app/routes/reviewRouter');
// const Drink = require('./app/model/drinkModel');

//khởi tạo ứng dụng nodejs
const app = new express();

app.use(cors());

// app.use(function (req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     next(); 
// });


//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//khai báo port chạy nodejs
const port = 8000;

const db = require('./config/db');

//Connect to DB
db.connect();

app.get('/', (request, response) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`);
    
    response.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

//sử dụng router
app.use('/', productTypeRouter);
app.use('/', productRouter);
app.use('/', customerRouter);
app.use('/', orderRouter);
app.use('/', orderDetailRouter);
app.use('/', reviewRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})